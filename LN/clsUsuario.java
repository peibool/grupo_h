package LN;

import COMUN.clsConstantesLN;
import COMUN.clsConstantesLN.en_ROL;

public class clsUsuario {
    
    private String str_nombre;
    private String str_nombreDiscord;
    private en_ROL str_rol;

    public clsUsuario()
    {
        this.str_nombre = "";
        this.str_nombreDiscord = "";
        this.str_rol = clsConstantesLN.en_ROL.en_ROL_INVALID;
    }

    public clsUsuario(String str_nombre, String str_nombreDiscord, 
    en_ROL str_rol)
    {
        this.str_nombre = str_nombre;
        this.str_nombreDiscord = str_nombreDiscord;
        this.str_rol = str_rol;
    }

    public String getNombre()
    {
        return this.str_nombre;
    }
    public void setNombre(String str_nombre)
    {
        this.str_nombre = str_nombre;
    }

    public String getNombreDiscord()
    {
        return this.str_nombreDiscord;
    }
    public void setNombreDiscord(String str_nombreDiscord)
    {
        this.str_nombreDiscord = str_nombreDiscord;
    }

    public en_ROL getDni()
    {
        return this.str_rol;
    }
    public void setDni(en_ROL str_rol)
    {
        this.str_rol = str_rol;
    }

    public String toString()
    {

        String str_retorno = "";
        str_retorno = this.str_nombre;
        str_retorno = str_retorno + " " + this.str_nombreDiscord;
        str_retorno = str_retorno + " " + this.str_rol;

        return str_retorno;

    } 
}

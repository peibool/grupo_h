package LN;

import java.util.ArrayList;

import COMUN.clsConstantesLN.en_ROL;

public class clsGestorLN {
    
    private ArrayList<clsUsuario>arrl_Usuarios;

    public clsGestorLN()
    {
        arrl_Usuarios = new ArrayList<clsUsuario>();
    }

    public LN.clsUsuario NuevaPersona(String str_nombre, String str_nombreDiscord, 
    en_ROL rol)
    {
        clsUsuario obj_Usuario;
        obj_Usuario = new clsUsuario(str_nombre, str_nombreDiscord, rol);
        arrl_Usuarios.add(obj_Usuario);
        return obj_Usuario;
    }
}